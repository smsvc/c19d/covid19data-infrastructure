# COVID-19 Data Infrastructure

This project provides the kubernetes manifests for the COVID-19 API.

It creates
* [covid19data-api](https://gitlab.com/smsvc/c19d/covid19data-api) (multiple versions)
* [covid19data-collector](https://gitlab.com/smsvc/c19d/covid19data-collector)
* [covid19data-landingpage](https://gitlab.com/smsvc/c19d/covid19data-landingpage)
* InfluxDB + PersistentVolumeClaim
* NGINX Proxy (as ingress)
* cert-manager issuer
* services for all components

## Prerequirements

* [NGINX Ingress Controller](https://kubernetes.github.io/ingress-nginx/)
* [cert-manager](https://cert-manager.io/)
	* LetsEncrypt issuer(s)
* [keel](https://keel.sh) (optional)
* [reloader](https://github.com/stakater/Reloader) (optional)

## Usage

`kubectl apply -f k8s-manifests`

## Traefik Ingress Configuration

Domain: c19d.smsvc.net
HTTPS enable via LetsEncrypt

---

All routing is done static via [ingressroute](k8s-manifests/ingress-nginx.yml):

* route `/` to [landingpage](https://gitlab.com/smsvc/c19d/landingpage) container
* route `/doc` and `/redoc` to [api v2](https://gitlab.com/smsvc/c19d/covid19data-api) container
* route `/api/v2` to [api v2](https://gitlab.com/smsvc/c19d/covid19data-api) container
